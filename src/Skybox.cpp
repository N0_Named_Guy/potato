#include "Skybox.hpp"
#include "TextureManager.hpp"
#include "ShaderManager.hpp"

extern TextureManager textureManager;
extern ShaderManager shaderManager;


Skybox::Skybox(void) {
}

void Skybox::loadGeometry(void) {
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	
	GLfloat vertices[] = {
		-1.0,  1.0,  1.0,
		-1.0, -1.0,  1.0,
		 1.0, -1.0,  1.0,
		 1.0,  1.0,  1.0,
		-1.0,  1.0, -1.0,
		-1.0, -1.0, -1.0,
		 1.0, -1.0, -1.0,
		 1.0,  1.0, -1.0,
	};

	GLushort elements[] = {
		3, 2, 1, 0,
		7, 6, 2, 3,
		4, 5, 6, 7,
		0, 1, 5, 4,
		4, 7, 3, 0,
		5, 6, 2, 1
	};

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);

	GLint position = glGetAttribLocation(skyShader, "position");
	glEnableVertexAttribArray(position);
	glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 0, 0);
}

void Skybox::setupCubeMap() {
    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_CUBE_MAP);
    glGenTextures(1, &cubemap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}


void Skybox::loadTexture(const std::string& skyname, const std::string& file, GLenum target) {
	std::string filepath = SKY_PATH + skyname + "/" + file;

	int width, height;
    unsigned char* image;

	image = SOIL_load_image(filepath.c_str(), &width, &height, 0, SOIL_LOAD_RGBA);
	if (!image) {
		d("<%s> sky file not found\n", filepath.c_str());
		return;
	}

    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap);
	
    glEnable(GL_TEXTURE_CUBE_MAP);
    glTexImage2D(
		target, 0,
		GL_RGBA,
		width, height,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE, image);

	SOIL_free_image_data(image);


}

void Skybox::loadSkybox(const std::string& skyname) {
	skyShader = shaderManager.getProgram("sky.json");
	viewUnif = glGetUniformLocation(skyShader, "view");
	projUnif = glGetUniformLocation(skyShader, "proj");

	loadGeometry();
	setupCubeMap();
	loadTexture(skyname, "left.png", GL_TEXTURE_CUBE_MAP_POSITIVE_X);
	loadTexture(skyname, "right.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
	loadTexture(skyname, "down.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
	loadTexture(skyname, "up.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
	loadTexture(skyname, "back.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);
	loadTexture(skyname, "front.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
}


void Skybox::render(const glm::mat4& proj, Camera& cam) {
	
	glDisable(GL_DEPTH_TEST);
	glBindVertexArray(vao);
	glUseProgram(skyShader);

	glm::mat4 view = cam.getView(false);

	glUniformMatrix4fv(viewUnif, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(projUnif, 1, GL_FALSE, glm::value_ptr(proj));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap);

	glDrawElements(GL_QUADS, 64, GL_UNSIGNED_SHORT, 0);

}

void Skybox::clean(void) {
	glDeleteTextures(1, &cubemap);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);	
	glDeleteVertexArrays(1, &vao);
}
