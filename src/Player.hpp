#ifndef POTATO_PLAYER_H
#define POTATO_PLAYER_H
#pragma once
#include "GameWorld.hpp"
#include "Unit.hpp"

class Player: public Unit {
public:
	int curCheckp;
	int laps;
	float hp;
	
	Player();

	virtual void loadPlayer(const std::string& playerFile);
	virtual void renderLogic(GameWorld* t, float interpolation);
	virtual void render(const glm::mat4& proj, const glm::mat4& view);
};
#endif /* POTATO_PLAYER_H */

