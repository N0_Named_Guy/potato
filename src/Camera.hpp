#ifndef POTATO_CAMERA_H
#define POTATO_CAMERA_H
#pragma once
#include "Entity.hpp"
#include "stdafx.h"

/// Defines a camera mode
enum CameraMode {
	/// <summary>Free camera that is unattached from any entity</summary>
	FREE,

	/// <summary>
	/// Moveable camera that follows an entity, from a fixed distance
	/// </summary>
	THIRD_PERSON,

	/// <summary>First person camera</summary>
	FIRST_PERSON
};


/// <summary>Defines the camera's light</summary>
struct CameraLight {
	GLfloat x, y, z;
	GLfloat dx, dy, dz;
};

class Camera: public Entity {

public:
	/// <summary>
	/// What entity should the camera follow.
	/// Not used in FREE camera mode
	/// </summary>
	const Entity* follow;
	
	/// <summary>
	/// The camera distance from the follow entity in THIRD_PERSON mode
	/// </summary>
	float followDist;

	/// <summary>The current camera mode</summary>
	CameraMode mode;
	Camera(void);
	~Camera(void);

	/// <summary>The current light</summary>
	CameraLight light;

	/// <summary>Returns the current view matrix</summary>
	glm::mat4 getView(void);
	glm::mat4 getView(const bool translate);

	/// <summary>Sets the camera into the free mode</summary>
	void freeCamera(void);

	/// <summary>Sets the camera to an entity's first person view</summary>
	/// <param name="ent">The entity to follow</param>
	void firstPerson(const Entity& ent);

	/// <summary>Sets the camera to an entity's third person view<summary>
	/// <param name="ent">The entity to follow</param>
	/// <param name="dist">The fixed distance to keep</param>
	void thirdPerson(const Entity& ent, float dist);
};

#endif /* POTATO_CAMERA_H */
