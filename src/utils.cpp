#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include "utils.hpp"

float randnum (float min, float max)
{
	int r;
    float	x;
    
	r = rand ();
    x = (float)(r & 0x7fff) /
		(float)0x7fff;
    return (x * (max - min) + min);
} 

void vectorDiff(const int d, const float* vec1, const float* vec2, float* ret) {
	for (int i = 0; i < d; i++) {
		ret[i] = vec1[i] - vec2[i];
	}
}

void normalize(const int d, float* vec) {
	float len = 0.0f;
	for (int i = 0; i < d; i++) {
		len += vec[i] * vec[i];
	}
	len = sqrtf(len);

	for (int i = 0; i < d; i++) {
		vec[i] /= len;
	}
}

void calcNormal(const float triangle[3][3], float normal[3]) {
	float u[3], v[3];
	
	vectorDiff(3, triangle[1], triangle[0], u);
	vectorDiff(3, triangle[2], triangle[1], v);

	normal[0] = -((u[1] * v[2]) - (u[2] * v[1]));
	normal[1] = -((u[0] * v[2]) - (u[2] * v[0]));
	normal[2] = -((u[0] * v[1]) - (u[1] * v[0]));

	normalize(3, normal);
}

char* readTextFile(const std::string& filename) {
	FILE* f = fopen(filename.c_str(), "rb");

    fseek(f, 0, SEEK_END);
	long size = ftell(f);
	fseek(f, 0, SEEK_SET);
	char* buff = new char[size + 1];
	fread(buff, sizeof(char), size, f);
	fclose(f);

    buff[size] = 0;
    return buff; 
}

float lerpf(float a, float b, float amt) {
	float diff = b - a;
	return a + (diff * amt);
}

Json::Value readJSONDoc(std::string file, Json::Reader& reader) {
	std::ifstream stream;
	Json::Value root;
	stream.open(file.c_str(), std::ifstream::in);

	reader.parse(stream, root, false);
	stream.close();

	return root;
}

void hexDump (const char *desc, const void *addr, const int len) {
    int i;
    unsigned char buff[17];
    unsigned char *pc = (unsigned char*)addr;

    // Output description if given.
    if (desc != NULL)
        printf ("%s:\n", desc);

    // Process every byte in the data.
    for (i = 0; i < len; i++) {
        // Multiple of 16 means new line (with line offset).

        if ((i % 16) == 0) {
            // Just don't print ASCII for the zeroth line.
            if (i != 0)
                printf ("  %s\n", buff);

            // Output the offset.
            printf ("  %04x ", i);
        }

        // Now the hex code for the specific character.
        printf (" %02x", pc[i]);

        // And store a printable ASCII character for later.
        if ((pc[i] < 0x20) || (pc[i] > 0x7e))
            buff[i % 16] = '.';
        else
            buff[i % 16] = pc[i];
        buff[(i % 16) + 1] = '\0';
    }

    // Pad out last line if not exactly 16 characters.
    while ((i % 16) != 0) {
        printf ("   ");
        i++;
    }

    // And print the final ASCII bit.
    printf ("  %s\n", buff);
}
