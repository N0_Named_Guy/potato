#ifndef POTATO_SCENE_H
#define POTATO_SCENE_H
#pragma once

#include "stdafx.h"
#include "Entity.hpp"
#include "Camera.hpp"
#include "Skybox.hpp"
#include "Terrain.hpp"
#include <list>

class Scene {
protected:
	std::list<Entity*> entities;

public:
	Skybox skybox;
	Camera camera;
	Terrain* terrain;

	void render(const glm::mat4&);
	void addEntity(Entity& ent);
    void removeEntity(Entity& ent);
	void clean(void);
};
#endif /* POTATO_SCENE_H */
