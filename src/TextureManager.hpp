#ifndef POTATO_TEXTURE_MANAGER_H
#define POTATO_TEXTURE_MANAGER_H
#pragma once

#include "stdafx.h"
#include <map>

class TextureManager {
protected:
	std::map<std::string, GLuint> textures;
public:
	TextureManager(void);
	GLuint loadTexture(const std::string& texture);
	GLuint loadTexture(const std::string& texture, bool absolute);
	GLuint loadTexture(const std::string& texture, bool absolute, int SOILFlags);

	void clearTextures();
};

#endif /* POTATO_TEXTURE_MANAGER_H */
