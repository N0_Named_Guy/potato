#ifndef POTATO_OBJ_MODEL_H
#define POTATO_OBJ_MODEL_H
#pragma once

#include "Model.hpp"

struct OBJFaceVertex {
	int vertexI, normalI, textureI;
	int materialI;
};

class OBJModel :
	public Model
{
protected:
	std::vector<Vertex> objVertices;
	std::vector<Coords3D> objNormals;
	std::vector<Coords3D> objTextures;
	std::vector<std::vector<OBJFaceVertex> > objFaces;

	virtual Material loadMaterial(std::string filename, std::string material);
	virtual void loadGeometry();
public:
	OBJModel(void);
	~OBJModel(void);

	int loadFile(std::string filename);
};
#endif /* POTATO_OBJ_MODEL_H */
