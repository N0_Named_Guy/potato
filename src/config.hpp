#ifndef POTATO_CONFIG_H
#define POTATO_CONFIG_H
#pragma once

#include "stdafx.h"

/// <summary>Default data path</summary>
#ifndef DATA_PATH
#define DATA_PATH "data/"
#endif

/// <summary>Default models path</summary>
#ifndef MODEL_PATH
#define MODEL_PATH DATA_PATH "models/"
#endif

/// <summary>Default game textures path</summary>
#ifndef TEXTURE_PATH
#define TEXTURE_PATH DATA_PATH "textures/"
#endif

/// <summary>Default texture extension</summary>
#ifndef TEXTURE_EXT
#define TEXTURE_EXT ".png"
#endif

/// <summary>Default shaders path</summary>
#ifndef SHADER_PATH
#define SHADER_PATH DATA_PATH "shaders/"
#endif

/// <summary>Default maps path</summary>
#ifndef MAP_PATH
#define MAP_PATH DATA_PATH "maps/"
#endif

/// <summary>Default skies path</summary>
#ifndef SKY_PATH
#define SKY_PATH DATA_PATH "sky/"
#endif

/// <summary>Default sounds path</summary>
#ifndef SOUND_PATH
#define SOUND_PATH DATA_PATH "sounds/"
#endif

/// <summary>Default player definitions path</summary>
#ifndef PLAYER_PATH
#define PLAYER_PATH DATA_PATH "players/"
#endif

/// <summary>Default user data path</summary>
#ifndef USER_PATH
#define USER_PATH "./"
#endif

/// <summary>Default player listing filename</summary>
#ifndef PLAYER_LIST
#define PLAYER_LIST "players.list"
#endif

/// <summary>Default configuration filename</summary>
#ifndef CONFIG_PATH
#define CONFIG_PATH USER_PATH "config.json"
#endif

/// <summary>Default starting map</summary>
#ifndef START_MAP
#define START_MAP "raceway.json"
#endif

/// <summary>Default player name</summary>
#ifndef PLAYER_NAME
#define PLAYER_NAME "Player"
#endif

/// <summary>Default screen width</summary>
#ifndef SCREEN_W
#define SCREEN_W 1280
#endif

/// <summary>Default screen height</summary>
#ifndef SCREEN_H
#define SCREEN_H 600
#endif

/// <summary>Default screen mode</summary>
#ifndef FULLSCREEN
#define FULLSCREEN false
#endif

/// <summary>Default show fps setting</summary>
#ifndef SHOW_FPS
#define SHOW_FPS false
#endif

/// <summary>Default game speed</summary>
#ifndef GAME_SPEED
#define GAME_SPEED 60
#endif

/// <summary>Default frames per second setting</summary>
#ifndef FPS
#define FPS 60
#endif

/// <summary>Default input latency</summary>
#ifndef INPUT_LATENCY
#define INPUT_LATENCY 0
#endif

/// <summary>Default net host</summary>
#ifndef NET_HOST
#define NET_HOST "127.0.0.1"
#endif

/// <summary>Default net port</summary>
#ifndef NET_PORT
#define NET_PORT 9666
#endif

/// <summary>Default net rate</summary>
#ifndef NET_RATE
#define NET_RATE FPS
#endif

/// <summary>Default controller keys</summary>
#ifndef CONTROLLER_KEYS
#define CONTROLLER_KEYS { \
	SDLK_UP, \
	SDLK_DOWN, \
	SDLK_LEFT, \
	SDLK_RIGHT, \
	SDLK_F1, \
	SDLK_r, \
	SDLK_RETURN, \
	SDLK_s, \
	SDLK_w, \
	SDLK_ESCAPE \
}
#endif


/// <summary>
/// The game's configuration class.
/// This class contains all the configurations loaded from default values or a
/// configuration file
/// </summary>
class Config {
public:
	/// <summary>The current screen width</summary>
	unsigned int screenW;

	/// <summary>The current screen height</summary>
	unsigned int screenH;

	/// <summary>The current game speed</summary>
	unsigned int gameSpeed;

	/// <summary>The current frames per second setting</summary>
	unsigned int fps;

	/// <summary>The current screen mode</summary>
	bool fullscreen;

	/// <summary>FPS counting</summary>
	bool showFPS;

	/// <summary>
	/// If set, the intro screen is skipped during the game's loading
	/// </summary>
	bool skipIntro;

	/// <summary>The current inital game</summary>
	std::string initialMap;

	/// <summary>The current player nickname</summary>
	std::string playerName;

	/// <summary>The current game controls</summary>
	Json::Value controls;

	/// <summary>The current net host</summary>
	std::string netHost;
	
	/// <summary>The current net host</summary>
	unsigned short netPort;

	/// <summary>The current network update rate</summary>
	unsigned int netRate;

	// Debug
	unsigned int minInputLatency;
	unsigned int maxInputLatency;
	unsigned int dropProbability;

	Config(void);
	~Config(void);
	
	/// <summary>
	/// Loads the game's configuration from the default configuration file
	/// </summary>
	void load(void);
	
	/// <summary>
	/// Loads a specific game configuration file
	/// </summary>
	/// <param name="file">The file's name</param>
	void load(std::string& file);
};

#endif /* POTATO_CONFIG_H */
