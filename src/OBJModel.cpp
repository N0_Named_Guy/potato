#include "OBJModel.hpp"
#include "TextureManager.hpp"

extern TextureManager textureManager;

OBJModel::OBJModel() {}
OBJModel::~OBJModel() {}

Material OBJModel::loadMaterial(std::string filename, std::string material) {
	Material toRet;
	toRet.ar = toRet.ag = toRet.ab = 0.2f;
	toRet.dr = toRet.dg = toRet.db = 0.8f;
	toRet.sr = toRet.sg = toRet.sb = 1.0f;
	toRet.alpha = 1.0f;
	toRet.shininess = 0.0f;
	toRet.illum = 1;
	toRet.tex = 0;

	FILE* f = fopen((MODEL_PATH + filename).c_str(), "r");
	char cmd[10];
	char matName[256];
	bool found = false;
	
	while (fscanf(f, "%s ",cmd) != EOF) {
		if (!strcmp(cmd, "newmtl")) {
			fscanf(f,"%[^\n]s", matName);
			found = !strcmp(matName, material.c_str());
		} else if (found) {

			if (!strcmp(cmd, "Ka")) {
				fscanf(f, "%f %f %f", &(toRet.ar), &(toRet.ag), &(toRet.ab));
			} else if (!strcmp(cmd, "Kd")) {
				fscanf(f, "%f %f %f", &(toRet.dr), &(toRet.dg), &(toRet.db));
			} else if (!strcmp(cmd, "Ks")) {
				fscanf(f, "%f %f %f", &(toRet.sr), &(toRet.sg), &(toRet.sb));
			} else if (!strcmp(cmd, "d") || !strcmp(cmd, "Tr")) {
				fscanf(f, "%f", &(toRet.alpha));
			} else if (!strcmp(cmd, "Ns")) {
				fscanf(f, "%f", &(toRet.shininess));
			} else if (!strcmp(cmd, "illum")) {
				fscanf(f, "%d", &(toRet.illum));
			} else if (!strcmp(cmd, "map_Kd")) {
				char mapname[1024];
				fscanf(f, "%[^\n]s", mapname);
				toRet.tex = textureManager.loadTexture(mapname);

			} else {
				fscanf(f, "%*[^\n]s");
			}
		} else {
			fscanf(f, "%*[^\n]s");
		}
	}

	fclose(f);
	return toRet;
}

int OBJModel::loadFile(std::string filename) {
	std::string filename_ = MODEL_PATH + filename;
	FILE* f = fopen(filename_.c_str(), "r");
	if (!f)	return -1;
	
	char cmd[10];
	int ret = 0;

	nElements = 0;

	char curMTLFile[256];
	char curMatName[256];
	Material curMaterial;

	while (fscanf(f, "%s ",cmd) != EOF) {
		
		if (!strcmp(cmd, "mtllib")) {
			fscanf(f, "%[^\n]s", curMTLFile);
		} else if (!strcmp(cmd, "usemtl")) {
			fscanf(f, "%[^\n]s", curMatName);
			curMaterial = loadMaterial(curMTLFile, curMatName);
			materials.push_back(curMaterial);

		// a vertex coords
		} else if (!strcmp(cmd, "v")) {
			Vertex vertex;
			fscanf(f, "%f %f %f", &vertex.x, &vertex.y, &vertex.z);

			vertex.nx = 0.0f;
			vertex.ny = 0.0f;
			vertex.nz = 0.0f;

			vertex.shared = 0;

			objVertices.push_back(vertex);
		}
		
		// a vertex texture coords (x,y,z) becomes (u,v,w)
		else if (!strcmp(cmd, "vt")) {
			Coords3D texCoords;
			int n = fscanf(f, "%f %f %f", &texCoords.x, &texCoords.y, &texCoords.z);
			if (n == 2) texCoords.z = 0;
			objTextures.push_back(texCoords);
		}
		
		// a vertex normal
		else if (!strcmp(cmd, "vn")) {
			Coords3D normal;
			fscanf(f, "%f %f %f", &normal.x, &normal.y, &normal.z);
			objNormals.push_back(normal);
		}

		// a polygon (a face)
		else if (!strcmp(cmd, "f")) {
			std::vector<OBJFaceVertex> face;
			std::vector<OBJFaceVertex> triangle;
			OBJFaceVertex fv;

			int n;
			while ((n = fscanf(f, "%d/%d/%d", &fv.vertexI, &fv.textureI, &fv.normalI)) > 0) {
				fv.textureI = (n >= 2 ? fv.textureI - 1 : -1);
				fv.normalI  = (n == 3 ? fv.normalI - 1 : -1);
				fv.materialI = materials.size() - 1;

				fv.vertexI -= 1;
				face.push_back(fv);
			}

			float triArr[3][3];
			float triNormal[3];

			for (unsigned int i = 1; i < face.size() - 1; i++) {

				triangle.clear();
				triangle.push_back(face[0]);
				triangle.push_back(face[i]);
				triangle.push_back(face[i + 1]);
				
				// Calculate normal
				for (unsigned int j = 0; j < triangle.size(); j++) {
					triArr[j][0] = objVertices[triangle[j].vertexI].x;
					triArr[j][1] = objVertices[triangle[j].vertexI].y;
					triArr[j][2] = objVertices[triangle[j].vertexI].z;
				}

				calcNormal(triArr, triNormal);
				Coords3D normal = {triNormal[0], triNormal[1], triNormal[2]};
				objNormals.push_back(normal);

				for (unsigned int j = 0; j < triangle.size(); j++) {
					if (triangle[j].normalI == -1) {
						triangle[j].normalI = objNormals.size() - 1;
					} else {
						normal = objNormals[triangle[j].normalI];
					}
					Vertex* v =  &objVertices[triangle[j].vertexI];
					v->nx += normal.x;
					v->ny += normal.y;
					v->nz += normal.z;
					v->shared++;
				}

				objFaces.push_back(triangle);
			}

		} else {
			fscanf(f, "%*[^\n]s");
			continue;
		}
	}
	
	// Average the normals
	std::vector<Vertex>::iterator vertexIt;
	for (vertexIt = objVertices.begin(); vertexIt != objVertices.end(); vertexIt++) {
		(*vertexIt).nx /= (*vertexIt).shared;
		(*vertexIt).ny /= (*vertexIt).shared;
		(*vertexIt).nz /= (*vertexIt).shared;
	}

	d("Vertices: %d  Normals: %u TextureCoords: %u Triangles: %u\n",
		(unsigned int)objVertices.size(),
        (unsigned int)objTextures.size(),
        (unsigned int)objNormals.size(),
        (unsigned int)objFaces.size()
    );

	fclose(f);
	loadGeometry();

	return ret;
}

void OBJModel::loadGeometry() {
#ifdef USE_VBOS

	// Init AABB
	aabb.maxX = aabb.maxY = aabb.maxZ = -std::numeric_limits<float>::infinity();
	aabb.minX = aabb.minY = aabb.minZ =  std::numeric_limits<float>::infinity();
	
	std::vector<std::vector<OBJFaceVertex> >::iterator facesIter;
	std::vector<OBJFaceVertex>::iterator vertexIter;

	nVertices = objFaces.size() * 3;
	// Every face has only 3 vertices
	vertices = new Vertex[nVertices];

	nElements = objFaces.size() * 3;
	unsigned int curI = 0;
	elements = new GLuint[nElements];

	// Make those indices!
	for (facesIter = objFaces.begin(); facesIter != objFaces.end(); facesIter++) {
		for (unsigned int i = 0; i < facesIter->size(); i++) {
			int vIndex = (*facesIter)[i].vertexI;
			int tIndex = (*facesIter)[i].textureI;

			Vertex* vertex = &objVertices[vIndex];
			Coords3D texCoords;
			if (tIndex > -1) {
				texCoords= objTextures[tIndex];
			} else {
				texCoords.x = texCoords.y = texCoords.z = 0.0f; 
			}

			aabb.maxX = SDL_max(aabb.maxX, vertex->x);
			aabb.maxY = SDL_max(aabb.maxY, vertex->y);
			aabb.maxZ = SDL_max(aabb.maxZ, vertex->z);

			aabb.minX = SDL_min(aabb.minX, vertex->x);
			aabb.minY = SDL_min(aabb.minY, vertex->y);
			aabb.minZ = SDL_min(aabb.minZ, vertex->z);

			vertices[curI] = *vertex;
			vertices[curI].s = texCoords.x;
			vertices[curI].t = 1.0f - texCoords.y;

			elements[curI] = curI;
			curI++;
		}
	}

	buildVBOs();

#endif
}
