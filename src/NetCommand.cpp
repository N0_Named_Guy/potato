#include "NetCommand.hpp"

NetCommand::NetCommand(const char* data, const unsigned int size) {
	this->data = new char[PACKET_SIZE];

	memcpy((void*)this->data, (void*)data, size);
	this->dataSize = size;

	char checkSum = calcChecksum(data, size);
	valid = (checkSum != data[size - 1]);
	fromPacket = true;
}

NetCommand::NetCommand(NetCommandID id) {
	this->data = new char[PACKET_SIZE];
    memset(this->data, 0, PACKET_SIZE);
	this->data[0] = (char)id; // Set command
	this->data[1] = 0; // Set number of args

	this->dataSize = PACKET_SIZE;
	this->data[PACKET_SIZE - 1] = calcChecksum(data, PACKET_SIZE);
	
	valid = true;
	fromPacket = false;
}

NetCommand::~NetCommand(void) {
	delete[] data;
}

char NetCommand::calcChecksum(const char* data, const unsigned int size) {
	return (char)0;
}


char* NetCommand::getData(void) {
	return &data[2];
}

char* NetCommand::getPacketData(void) {
	return data;
}

int NetCommand::getDataSize(void) {
	return dataSize;
}

bool NetCommand::isValid(void) {
	return valid;
}

char* NetCommand::getArgPointer(const char n) {
	char nArgs = n;
	char* pointer = &data[2];

	while (nArgs) {
		for (;*(pointer++);) {}
		nArgs--;
	}

	return pointer;
}

char NetCommand::addArgument(const char* arg, const unsigned int size) {
	char* pointer = getArgPointer(getArgumentCount());

	memcpy(pointer, arg, size);
	pointer[size] = 0;

	data[1]++;
	return getArgumentCount();
}

char NetCommand::addArgument(const char arg) {
	char* pointer = getArgPointer(getArgumentCount());
	pointer[0] = arg;
	pointer[1] = 0;

	data[1]++;
	return getArgumentCount();
}

char NetCommand::addArgument(const std::string& arg) {
	return addArgument(arg.c_str(), arg.length());
}

char* NetCommand::getArgument(const char n) {
	if (n >= getArgumentCount()) {
		return NULL;
	}

	return getArgPointer(n);
}

char NetCommand::getArgumentCount(void) {
	return data[1];
}

NetCommandID NetCommand::getCommand(void) {
	return (NetCommandID)data[0];
}

void NetCommand::print(void) {
	int count = getArgumentCount();
	int n = count;

	d("[0x%x] ", getCommand());
	for (;n--;) {
		d("%s ",getArgument(count - n));
	}
	d("%s", "\n");
}
