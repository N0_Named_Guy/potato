#include "PlayerSelect.hpp"
#include "SurfaceManager.hpp"
#include "config.hpp"

#include <fstream>

extern Config cfg;
extern SurfaceManager surfaceManager;

PlayerSelect::PlayerSelect(std::string startMap) {
	rotation = 0.0f;
}

void PlayerSelect::setup(void) {
	loadPlayerList();
	setupPlayers();

	glViewport(0, 0, cfg.screenW, cfg.screenH);

	surf = surfaceManager.getSurface("pointer_back.png");
}

void PlayerSelect::setupPlayers(void) {
	int n = players.size();
	int i = 0;
	float angle;

	for (std::vector<Player>::iterator it = players.begin(); it != players.end(); it++) {
		scene.addEntity(*it);
		angle = ((float)i / (float)n) * 360.0f * PIOVER180;
		(*it).setLocation(RADIUS * sinf(angle), 0.0f, RADIUS * cosf(angle));
		(*it).yaw = angle / PIOVER180;
		i++;
	}

	scene.camera.freeCamera();
	scene.camera.setLocation(0.0f, 0.0f, 0.0f);
	scene.skybox.loadSkybox("strand");
}

void PlayerSelect::processInput(float dt) {
	control.poll();
	if (control.isDown(turnLeft)) {
		scene.camera.turnRight(-4.0f * dt);
	} else if (control.isDown(turnRight)) {
		scene.camera.turnRight(4.0f * dt);
	}

	if (control.isDown(forward)) {
		scene.camera.forward(0.1f * dt);
	} else if (control.isDown(backward)) {
		scene.camera.forward(-0.1f * dt);
	}
}

void PlayerSelect::loadPlayerList(void) {
	std::string list = PLAYER_PATH;
	list += PLAYER_LIST;
	char buff[256];
	std::string sbuff;

	std::ifstream in;
	in.open(list.c_str());

	Player p;

	while (!in.eof()) {
		in.getline(buff, 256);
		sbuff = std::string(buff);
		p.loadPlayer(sbuff);
		players.push_back(p);
	}

	in.close();
}

void PlayerSelect::renderScene(void) {
	glm::mat4 proj;

	glClear(GL_COLOR_BUFFER_BIT);
	proj = glm::perspective(45.0f, (float)cfg.screenW / (float)cfg.screenH, 0.1f, 100.0f); 
    scene.render(proj);


	SDL_GL_SwapBuffers();
}
