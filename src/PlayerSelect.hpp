#ifndef POTATO_PLAYER_SELECT
#define POTATO_PLAYER_SELECT
#pragma once

#include "World.hpp"
#include "GLSurface.hpp"
#include "GameWorld.hpp"
#include "Player.hpp"
#include "Scene.hpp"
#include "Controller.hpp"

#define RADIUS 20.0f

class PlayerSelect :
	public World
{
protected:

	Scene scene;
	GLSurface surf;
	GameWorld* nextWorld;
	std::string gameWorld;

	std::vector<Player> players;

	Controller control;
	int curPlayer;

	float rotation;

	virtual void loadPlayerList(void);
	virtual void setupPlayers(void);
	
public:
	PlayerSelect(std::string startMap);
	virtual void setup(void);
	virtual void processInput(float dt);
	virtual void renderScene(void);
};
#endif /* POTATO_PLAYER_SELECT */
