#include "ShaderManager.hpp"

GLuint createShader(const std::string& filename) {
	d("<%s> Loading shader\n", filename.c_str());

    GLenum shaderType;
    size_t found = filename.find_last_of(".");
    std::string ext = filename.substr(found + 1);

    if (ext.compare("vert") == 0) {
        shaderType = GL_VERTEX_SHADER;
    } else if (ext.compare("frag") == 0) {
        shaderType = GL_FRAGMENT_SHADER;
    } else {
        return 0;
    }
    
	GLuint shader = glCreateShader(shaderType);
	std::string fullFileName = SHADER_PATH + filename;
	const char* shaderSrc = readTextFile(fullFileName);
	glShaderSource(shader, 1, &shaderSrc, NULL);

    delete[] shaderSrc;

	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {
		GLint infoLogLen;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLen);

		GLchar* infoLog = new GLchar[infoLogLen + 1];
		glGetShaderInfoLog(shader, infoLogLen, NULL, infoLog);

		const char* strShaderType = NULL;
		switch (shaderType) {
		case GL_VERTEX_SHADER: strShaderType = "vertex"; break;
		case GL_GEOMETRY_SHADER: strShaderType = "geometry"; break;
		case GL_FRAGMENT_SHADER: strShaderType = "fragment"; break;
		}

		d("<%s>[%s]: %s\n", filename.c_str(), strShaderType, infoLog);
		delete[] infoLog;

        return 0;
	}

	return shader;
}

GLuint createProgram(const Json::Value& shadersNode) {
	
	GLuint program = glCreateProgram();
	unsigned int nShaders = shadersNode.size();
	
	for (unsigned int i = 0; i < nShaders; i++) {
		std::string shaderName = shadersNode[i].asString();
		GLuint shader = createShader(shaderName);
        if (shader > 0) {
		    glAttachShader(program, shader);
        } else {
            d("<%s> shader failed to compile, program compilation failed\n",
                shaderName.c_str());
            return 0;
        }
	}
	glLinkProgram(program);

	return program;
}

GLuint ShaderManager::getShader(const std::string& filename) {
	GLuint shader = shaders[filename];
	if (!shader) {
		shader = createShader(filename);
		shaders[filename] = shader;
	}
	
	return shader;
}

GLuint ShaderManager::getProgram(const std::string& filename) {
	GLuint program = programs[filename];
	if (!program) {
        unsigned int nPrograms;

		d("<%s> Loading shader program\n", filename.c_str());

		std::string fullFileName = SHADER_PATH + filename;

		Json::Value root;
		Json::Value shadersNode;
		Json::Reader reader;
    
		root = readJSONDoc(fullFileName, reader);
        shadersNode = root.get("shaders", "");
        nPrograms = shadersNode.size();
        
        for (unsigned int i = 0; i < nPrograms; i++) {
            program = createProgram(shadersNode[i]);
		    if (program > 0) {
                break;
            }
        }
		
		programs[filename] = program;
	}
	return program;
}

void ShaderManager::freeAllShaders(void) {
	for (
        std::map<std::string, GLuint>::iterator it = shaders.begin();
        it != shaders.end();
        it++
    ) {
		glDeleteShader((*it).second);
	}
}

void ShaderManager::freeAllPrograms(void) {
	freeAllShaders();
	for (
        std::map<std::string, GLuint>::iterator it = programs.begin();
        it != programs.end();
        it++
    ) {
		glDeleteProgram((*it).second);
	}
}
