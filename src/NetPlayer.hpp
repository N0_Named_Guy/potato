#ifndef POTATO_NET_PLAYER_H
#define POTATO_NET_PLAYER_H
#pragma once

#include "Player.hpp"
#include "NetSocket.hpp"
#include "NetController.hpp"

class NetPlayer : public Player
{
public:
    NetController control;
    NetAddress addr;
    std::string name;
    unsigned int id;
    bool ready;

    NetPlayer(const std::string name, NetAddress addr, unsigned int id);
};

#endif /* POTATO_NET_PLAYER_H */
