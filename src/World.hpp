#ifndef POTATO_WORLD_H
#define POTATO_WORLD_H
#pragma once

#include "stdafx.h"

class World {
protected:
    GLuint shaderProgram;
    glm::mat4 view;
    glm::mat4 model;
    glm::mat4 projection;

public:
	World* nextWorld;
	bool running;

	World(void);
    virtual ~World(void);
	
	virtual void setup(void);
	virtual void processInput(float interpolation);
	virtual void processLogic(float interpolation);
	virtual void renderScene(void);
	virtual void cleanup(void);
};

#endif /* POTATO_WORLD_H */
