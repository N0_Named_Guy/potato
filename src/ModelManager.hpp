#ifndef POTATO_MODEL_MANAGER_H
#define POTATO_MODEL_MANAGER_H
#pragma once

#include "OBJModel.hpp"
#include "stdafx.h"
#include <map>

class ModelManager {
protected:
	std::map<std::string, Model*> pointers;
public:
	Model* getOBJModel(std::string filename);
	void freeAllModels();
};
#endif /* POTATO_MODEL_MANAGER_H */
