#ifndef POTATO_MODEL_ENTITY_H
#define POTATO_MODEL_ENTITY_H
#pragma once

#include "Entity.hpp"
#include "ModelManager.hpp"

class ModelEntity :
	public Entity
{
public:
	ModelEntity() { modelPointer = NULL; }
	ModelEntity(Model* model);
	Model* modelPointer;

	virtual void render(const glm::mat4& proj, const glm::mat4& view);
	
	virtual bool collide(ModelEntity& ent);
	virtual bool collide(ModelEntity& ent, float nx, float ny, float nz);
};
#endif /* POTATO_MODEL_ENTITY_H */
