/*
	Simple Network Library from "Networking for Game Programmers"
	http://www.gaffer.org/networking-for-game-programmers
	Author: Glenn Fiedler <gaffer@gaffer.org>

	Adapted by David Serrano	
*/
#ifndef POTATO_NET_SOCKET_H
#define POTATO_NET_SOCKET_H
#pragma once

#include <string>
#include "NetCommand.hpp"

class NetAddress {
protected:
	unsigned int address;
	unsigned short port;
public:

	NetAddress(void);
	NetAddress(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port);
	NetAddress(unsigned int address, unsigned short port);
	NetAddress(const std::string& address, unsigned short port);
	unsigned int getAddress(void) const;
	unsigned char getA(void) const;
	unsigned char getB(void) const;
	unsigned char getC(void) const;
	unsigned char getD(void) const;
	unsigned short getPort(void) const;

    struct sockaddr_in getSockAddr(void);

	bool operator == (const NetAddress & other) const;
	bool operator != (const NetAddress & other) const;
};

struct NetAddressHasher {
    std::size_t operator()(const NetAddress& addr) const {
        return (std::hash<int>()(addr.getAddress())) ^
            (std::hash<short>()(addr.getPort() << 1));
    }
};

class NetSocket {
protected:
    int handler;

public:
    NetSocket(void);
    ~NetSocket(void);

    bool open(void);
    void close(void);
	bool setNonblock(bool nonBlock);
    bool bind(const NetAddress& addr);

	bool isOpen(void) const;

};   

class NetClientSocket : public NetSocket {
public:
    bool connect(NetAddress& addr);

    int receive(char* data, size_t len);
    int send(char* data, size_t len);
    bool send(NetCommand& cmd);
    NetCommand receiveCommand(void);
};

class NetServerSocket : public NetSocket {
public:
    bool listen(unsigned const short port);

	int receive(NetAddress & sender, void * data, size_t size);
    int send(const NetAddress& addr, char* data, size_t len);
    bool send(const NetAddress& addr, NetCommand& cmd);
};

extern bool net_init(void);
extern void net_cleanup(void);

#endif /* POTATO_NET_SOCKET_H */
