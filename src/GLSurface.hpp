#ifndef POTATO_GL_SURFACE_H
#define POTATO_GL_SURFACE_H
 
#pragma once
#include "stdafx.h"
#include "Entity.hpp"
#include "TextureManager.hpp"
#include "ShaderManager.hpp"
#include <list>

enum SurfEffect {
	FADE_IN, FADE_OUT, IDLE
};

struct SurfAnimation {
	SurfEffect effect;
	unsigned int duration;
};

class GLSurface: public Entity {
protected:
	GLuint tex;

	std::list<SurfAnimation> qEffects;

	unsigned int lastTicks;
	unsigned int animTime;

	GLuint vao;
	GLuint shaderProgram;
	GLuint projUnif;
	GLuint modelUnif;
	GLuint colorUnif;

    virtual void setupTextures(void);
	void parseAnim(void);
	void addAnimation(SurfEffect e, unsigned int ms);

public:
	glm::vec4 color;
	SurfAnimation lastAnimation;

	GLSurface() {};
	GLSurface(const std::string& texture,
		const GLuint vao, const GLuint shader,
		const GLuint proj, const GLuint model,
		const GLuint color);

	bool animating;

	void stopAnimation(void);
	void fadeIn(unsigned int duration);
	void fadeOut(unsigned int duration);
	void idle(unsigned int duration);

    virtual void render(void) override;
    virtual void render(const glm::mat4& proj) override;
};

#endif /* POTATO_GL_SURFACE_H */
