#ifndef POTATO_NET_COMMAND_H
#define POTATO_NET_COMMAND_H
#pragma once

#include "stdafx.h"
#define PACKET_SIZE 256

enum NetCommandID {
	INVALID = 0x00,
	PING = 0x01,
	PONG = 0x02,
	
	CONNECT = 0x10,
	READY = 0x11,
	QUIT = 0x12,

	LEVEL = 0x20,
	ACCEPT = 0x21,
	JOIN = 0x22,
	PART = 0x23,

	CONTROLLER = 0x30,
	
	GAMESTATE = 0x40
};

class NetCommand
{
protected:
	char* data;
	unsigned int dataSize;
	bool fromPacket;
	bool valid;

	char calcChecksum(const char* data, const unsigned int size);
	char* getArgPointer(const char n);

public:
	NetCommand(const char* data, const unsigned int size);
	NetCommand(NetCommandID id);

	~NetCommand(void);

	char addArgument(const char* const data, const unsigned int size);
	char addArgument(const char data);
	char addArgument(const std::string& data);
	char* getArgument(const char n);
	char getArgumentCount(void);

	NetCommandID getCommand(void);
	char* getData(void);
	char* getPacketData(void);
	int getDataSize(void);
	bool isValid(void);

	void print(void);
};
#endif /* POTATO_NET_COMMAND_H */
