#ifndef POTATO_MODEL_H
#define POTATO_MODEL_H
#pragma once

#include "stdafx.h"
#include "Entity.hpp"
#include "ShaderManager.hpp"
#include <vector>

struct Coords3D {
	GLfloat x, y, z;
};

struct Vertex {
	GLfloat x, y, z;        //Vertex
	GLfloat nx, ny, nz;     //Normal
	float s, t;         //Texcoord0
/*	float s1, t1;         //Texcoord1
	float s2, t2;         //Texcoord2 */

	unsigned int shared; // The number of times this vertice has been shared
};

struct BoundingBox {
	float minX, maxX;
	float minY, maxY;
	float minZ, maxZ;
};

struct Material {
	float ar, ag, ab; // ambience
	float dr, dg, db; // diffuse
	float sr, sg, sb; // specular
	float shininess;
	float alpha;
	// illumination model
	// - 1 indicates a flat material with no specular highlights
	// - 2 denotes the presence of specular highlights
	int illum;
	GLuint tex;
};

class Model:
	public Entity {
protected:
	GLuint vao;
	GLuint vbo;
	GLuint ebo;

	GLuint shader;
	
	Vertex* vertices;
	int nVertices;
	
	GLuint* elements;
	int nElements;

	std::vector<Material> materials;

	GLuint projUnif;
	GLuint viewUnif;
	GLuint modelUnif;
	GLuint texUnif;


	virtual void buildVBOs();
public:
	BoundingBox aabb;

	Model(void);
    virtual ~Model(void);
	virtual void clean(void);
	
	virtual void render(const glm::mat4& proj, const glm::mat4& view);
	virtual void render(const glm::mat4& proj, const glm::mat4& view, const glm::mat4& model);
};
#endif /* POTATO_MODEL_H */
