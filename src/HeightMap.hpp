#ifndef POTATO_HEIGHT_MAP_H
#define POTATO_HEIGHT_MAP_H

#pragma once
#include "stdafx.h"
#include "Model.hpp"
#include "TextureManager.hpp"

extern TextureManager textureManager;

#define POS(x, y, w) ((y) * (w) + (x))

class HeightMap :
	public Model
{
protected:
	unsigned char* heightMap;
	int mWidth, mHeight;

	float minH, maxH, scale;
	int detail;

	virtual GLuint* makeElementsArray(int step, int x, int y, int w, int h);
	virtual GLuint* makeElementsArray(int x, int y, int w, int h);
	virtual float getHeightAt(const int at[2]);
	virtual float getHeightAt(const int x, const int z);
	virtual void loadGeometry();
	
public:
	HeightMap(void);
	~HeightMap(void);
	
	void loadFromPNG(const std::string& filename,
		const std::string& textureFile,
		float minH, float maxH, float scale, int detail);
	
	float getHeight(float x, float z);
	void render(const glm::mat4& proj, const glm::mat4& view);
};
#endif /* POTATO_HEIGHT_MAP_H */
