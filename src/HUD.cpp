#include "HUD.hpp"

extern TextureManager textureManager;
extern SurfaceManager surfaceManager;
extern Config cfg;

void HUD::setup(void) {
	surfPointer = surfaceManager.getSurface("pointer.png");
	surfPointerBack = surfaceManager.getSurface("pointer_back.png");
	surfGameOver = surfaceManager.getSurface("gameover.png");
	surfWin = surfaceManager.getSurface("win.png");
	surfHP = surfaceManager.getSurface("hp_bar.png");
	surfHPBack = surfaceManager.getSurface("hp_bar_back.png");
	surfCheckpoint = surfaceManager.getSurface("checkpoint.png");

	const float diameter = 0.3f;
	surfPointerBack.sx = surfPointer.sx = diameter;
	surfPointerBack.sy = surfPointer.sy = diameter;
	
	surfPointerBack.x = surfPointer.x = 0.0f;
	surfPointerBack.y = surfPointer.y = -0.3f;

	surfHPBack.sy = surfHP.sy = 0.05f;
	surfHPBack.y = surfHP.y = -0.48f;

	surfCheckpoint.sx = 0.5f;
	surfCheckpoint.sy = 0.1f;
	surfCheckpoint.y = 0.3f;
	surfCheckpoint.color.a = 0.0f;
}


void HUD::renderSpeed(float speed, const glm::mat4& proj) {
	const float minAngle = 135.00f;
	const float maxAngle = -135.00f;

	float angle = (((minAngle) + (speed * (maxAngle - minAngle))));// * PIOVER180;
	
	surfPointer.roll = angle;

	surfPointerBack.render(proj);
	surfPointer.render(proj);
}

void HUD::render(HUDData& data) {
	glm::mat4 proj;
	proj = glm::ortho(-0.5f, 0.5f, -0.5f, 0.5f);

	if (cfg.screenW > cfg.screenH) {
		glViewport((cfg.screenW - cfg.screenH) / 2, 0, cfg.screenH, cfg.screenH);
	} else {
		glViewport(0, (cfg.screenH - cfg.screenW) / 2, cfg.screenW, cfg.screenW);
	}
	
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	renderSpeed(data.speed, proj);
	surfHPBack.render(proj);

	if (data.checkpoint) {
		surfCheckpoint.stopAnimation();
		surfCheckpoint.fadeIn(1000);
		surfCheckpoint.idle(3000);
		surfCheckpoint.fadeOut(3000);
		data.checkpoint = false;
	}

    if (surfCheckpoint.animating) {
        surfCheckpoint.render(proj);
    }

	surfHP.sx = data.hp;
	surfHP.render(proj);

	if (data.state == gameover) { 
		glViewport(0, 0, cfg.screenW, cfg.screenH);
		surfGameOver.render(proj);
	} else if (data.state == win) {
		glViewport(0, 0, cfg.screenW, cfg.screenH);
		surfWin.render(proj);
	}

}
