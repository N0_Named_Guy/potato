#ifndef POTATO_NET_CLIENT_WORLD
#define POTATO_NET_CLIENT_WORLD
#pragma once

#include "GameWorld.hpp"
#include "NetSocket.hpp"
#include "NetController.hpp"
#include "NetCommand.hpp"
#include "NetPlayer.hpp"

#include "SurfaceManager.hpp"
#include "GLSurface.hpp"

#include "Timer.hpp"

#define CONNECT_PORT 9666

enum NetClientStatus {
	DISCONNECTED = 0x0,
	CONNECTING = 0x1,
	WAITING = 0x2,
	LOADING = 0x3,
	PLAYING = 0x4
};

class NetClientWorld :
	public GameWorld {

protected:
	NetClientSocket socket;
	NetAddress serverAddr;
	NetController netControl;
	GLSurface connectSurf;
	
	NetClientStatus status;

	Timer netTimer;

	NetPlayer* player;
	bool firstPerson;

	void renderConnecting(void);
	virtual void loadWorld(const std::string& worldFile);

	virtual void setupGraphics(void);

	virtual void parseCommand(NetCommand& cmd);

	virtual void processPlayer(float dt);
	virtual void processNetwork(float dt);
public:
	NetClientWorld(void);

	virtual void setup(void);
	virtual void cleanup(void);

	virtual void processInput(float dt);
	virtual void processLogic(float dt);
	virtual void renderScene(void);

	bool reconnect();
	bool connectTo(NetAddress addr);
};
#endif /* POTATO_NET_CLIENT_WORLD */
