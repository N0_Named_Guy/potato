#include "Controller.hpp"

Controller::~Controller(void) {}

std::string Controller::getActionString(int action) {
	return actionsStr[action];
}

int Controller::getActionFromString(std::string& str) {
	for (int i = 0; i < nActions; i++) {
		if (str.compare(actionsStr[i]) == 0) {
			return i;
		}
	}
	return -1;
}

void Controller::poll(void) {}

bool Controller::isDown(ControllerActions action) {
	return actions[action];
}

bool Controller::isPressed(ControllerActions action) {
	bool ret = isDown(action);
	actions[action] = false;
	return ret;
}

bool Controller::press(ControllerActions action) {
	bool ret = isDown(action);
	actions[action] = true;
	return ret;
}

bool Controller::depress(ControllerActions action) {
	bool ret = isDown(action);
	actions[action] = false;
	return ret;
}

bool Controller::toggle(ControllerActions action) {
	bool ret = isDown(action);
	actions[action] = !ret;
	return ret;
}

void Controller::copy(Controller& control) {
	if (nActions) {
		delete[] actions;
	}
	
	axis = control.axis;
	nActions = control.nActions;
    actions = new bool[nActions];

	memcpy(actions, control.actions, sizeof(bool) * control.nActions);
}

void Controller::clean(void) {}
void Controller::init(void) {}

void Controller::resetAxis(void) {
	axis.x = axis.y = 0;
}

Axis Controller::getAxis(void) {
	return axis;
}
