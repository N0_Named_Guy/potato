#ifndef POTATO_NET_SERVER_WORLD_H
#define POTATO_NET_SERVER_WORLD_H
#pragma once

#include "stdafx.h"
#include "GameWorld.hpp"
#include "NetController.hpp"
#include "NetSocket.hpp"
#include "NetCommand.hpp"
#include "NetPlayer.hpp"

#include <unordered_map>

class NetServerWorld :
	public GameWorld
{
protected:
	NetServerSocket socket;
	bool mouseView;
	
	virtual void parseCommand(NetCommand& cmd, NetAddress& addr);
    std::unordered_map<NetAddress, NetPlayer*, NetAddressHasher> players;
	char nextPlayerId;

    virtual NetPlayer* getPlayerByAddr(const NetAddress& addr);

    virtual void connectPlayer(NetCommand& cmd, const NetAddress& addr);
	virtual char createPlayer(const std::string& name, const NetAddress& addr);
    virtual void readyPlayer(const NetAddress& addr);
    virtual void removePlayer(const NetAddress& addr);
    virtual void updatePlayer(NetCommand& cmd, const NetAddress& addr);

	virtual void processNetwork(float dt);
    virtual void processPlayers(float dt); 
public:
	NetServerWorld(void);

	virtual void setup(void);
	virtual void cleanup(void);

	virtual void processLogic(float dt);
	virtual void processInput(float dt);
	virtual void loadWorld(const std::string& world);
	virtual void renderScene();
};
#endif /* POTATO_NET_SERVER_WORLD_H */
