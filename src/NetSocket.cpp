#include "NetSocket.hpp"
#include "stdafx.h"

#if PLATFORM == PLATFORM_WINDOWS
#include <winsock2.h>
#elif PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>

#endif

bool net_init(void) {
#if PLATFORM == PLATFORM_WINDOWS
	WSADATA WsaData;
	return WSAStartup(MAKEWORD(2,2), &WsaData) == NO_ERROR;
#else
	return true;
#endif
}

void net_cleanup(void) {
#if PLATFORM == PLATFORM_WINDOWS
	WSACleanup();
#endif
}

NetAddress::NetAddress(void) {
	port = address = 0;
}

NetAddress::NetAddress(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port) {
	this->address = ( a << 24 ) | ( b << 16 ) | ( c << 8 ) | d;
	this->port = port;
}

NetAddress::NetAddress(unsigned int address, unsigned short port) {
	this->address = address;
	this->port = port;
}

NetAddress::NetAddress(const std::string& address, unsigned short port) {
    hostent* host;
    host = gethostbyname(address.c_str());

    in_addr* addr = (in_addr*)host->h_addr;

    this->address = htonl(addr->s_addr);
    this->port = port;
#ifdef NET_DEBUG
    d("[NET] Resolved to %d.%d.%d.%d\n", getA(), getB(), getC(), getD());
#endif
}

unsigned int NetAddress::getAddress(void) const {
	return this->address;
}

unsigned char NetAddress::getA(void) const {
	return (unsigned char)(address >> 24);
}
	
unsigned char NetAddress::getB(void) const {
	return (unsigned char)(address >> 16);
}
	
unsigned char NetAddress::getC(void) const {
	return (unsigned char)(address >> 8);
}
	
unsigned char NetAddress::getD(void) const {
	return (unsigned char)(address);
}
	
unsigned short NetAddress::getPort(void) const { 
	return port;
}
	
struct sockaddr_in NetAddress::getSockAddr(void) {
    struct sockaddr_in ret;
    memset(&ret, 0, sizeof(struct sockaddr_in));

    ret.sin_family = AF_INET;
    ret.sin_port = htons(getPort());
    ret.sin_addr.s_addr = htonl(getAddress());

    return ret;
}

bool NetAddress::operator == (const NetAddress& other) const {
	return address == other.address && port == other.port;
}
	
bool NetAddress::operator != (const NetAddress & other) const {
	return ! ( *this == other );
}

NetSocket::NetSocket(void) {
	handler = 0;
}

NetSocket::~NetSocket(void) {
	this->close();
}

bool NetSocket::open(void) {
	// create socket
	handler = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );

	if ( socket <= 0 ) {
#ifdef NET_DEBUG
		d("[NET] %s\n", "Could not open socket");
#endif
		handler = 0;
		return false;
	}

    return this->setNonblock(true);
}

void NetSocket::close(void) {
	if (handler != 0) {
#if PLATFORM == PLATFORM_WINDOWS
		closesocket(handler);
#else
		::close(handler);
#endif
#ifdef NET_DEBUG
		d("[NET] %s\n", "Closing socket");
#endif
		handler = 0;
	}
}

bool NetSocket::setNonblock(bool nonBlock) {
#if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
		
	int nonBlocking = (int)nonBlock;
	if (fcntl( handler, F_SETFL, O_NONBLOCK, nonBlocking ) == -1) {
		this->close();
		return false;
	}
			
#elif PLATFORM == PLATFORM_WINDOWS
		
	DWORD nonBlocking = (DWORD)nonBlock;
	if (ioctlsocket(handler, FIONBIO, &nonBlocking) != 0) {
		this->close();
		return false;
	}

#endif

	return true;
}

// FIXME: Allow IP bindings
bool NetSocket::bind(const NetAddress& addr) {
	sockaddr_in address;

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons( (unsigned short) addr.getPort() );
		
	if ( ::bind( handler, (const sockaddr*) &address, sizeof(sockaddr_in) ) < 0) {
#ifdef NET_DEBUG
		d("[NET] %s\n", "Could not bind socket");
#endif
		this->close();
		return false;
	}
    return true;
}

bool NetSocket::isOpen(void) const {
	return handler != 0;
}


bool NetClientSocket::connect(NetAddress& addr) {
    if (!this->open()) {
        return false;
    }

    struct sockaddr_in addr_in = addr.getSockAddr();

    ::connect(handler, (sockaddr*)&addr_in, sizeof(struct sockaddr_in));

    return true;
}

int NetClientSocket::receive(char* data, size_t size) {
	if ( handler == 0 )
		return 0;
			
#if PLATFORM == PLATFORM_WINDOWS
	typedef int socklen_t;
#endif
			
	int received_bytes = recv(handler, (char*)data, size, 0);

	if ( received_bytes <= 0 )
		return 0;

#ifdef NET_DEBUG
	d("[NET] Recv %d bytes from server\n", received_bytes);
#ifdef NET_DUMP_HEX
    hexDump("Received", data, received_bytes); 
#endif
#endif

	return received_bytes;
}

int NetClientSocket::send(char* data, size_t len) {
#ifdef NET_DEBUG
	d("[NET] Sent %zu bytes\n", len);
#ifdef NET_DUMP_HEX
    hexDump("Sent", data, len); 
#endif
#endif
    return ::send(handler, data, len, 0);
}

bool NetClientSocket::send(NetCommand& cmd) {
    int ret = send(cmd.getPacketData(), PACKET_SIZE);
    return ret == PACKET_SIZE; 
}

bool NetServerSocket::listen(unsigned const short port) {
    if (!this->open()) {
        return false;
    }

	// bind to port
    if (!this->bind(NetAddress(0, 0, 0, 0, port))) {
        return false;
    }

	return true;
}

int NetServerSocket::receive(NetAddress& sender, void* data, size_t size) {

	if ( handler == 0 )
		return 0;
			
#if PLATFORM == PLATFORM_WINDOWS
	typedef int socklen_t;
#endif
			
	sockaddr_in from;
	socklen_t fromLength = sizeof( from );

	int received_bytes = recvfrom(handler,
        (char*)data, size, 0,
        (sockaddr*)&from, &fromLength );

	if ( received_bytes <= 0 )
		return 0;

	unsigned int address = ntohl( from.sin_addr.s_addr );
	unsigned int port = ntohs( from.sin_port );

	sender = NetAddress( address, port );

#ifdef NET_DEBUG
	d("[NET] Recv %d bytes from %d.%d.%d.%d:%d\n", received_bytes,
		sender.getA(), sender.getB(), sender.getC(), sender.getD(),
		sender.getPort());
#ifdef NET_DUMP_HEX
    hexDump("Received", data, received_bytes); 
#endif
#endif

	return received_bytes;
}

int NetServerSocket::send(const NetAddress& dest, char* data, size_t len) {
	sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(dest.getAddress());
    addr.sin_port = htons((unsigned short)dest.getPort());

	int ret = ::sendto(handler, data, len, 0, (sockaddr*)&addr, sizeof(sockaddr_in));
	d("[NET] Recv %d bytes from %d.%d.%d.%d:%d\n", ret,
		dest.getA(), dest.getB(), dest.getC(), dest.getD(),
		dest.getPort());
	
	return ret;
}

bool NetServerSocket::send(const NetAddress& addr, NetCommand& cmd) {
    int ret = send(addr, cmd.getPacketData(), PACKET_SIZE);
	return ret == PACKET_SIZE;
}
