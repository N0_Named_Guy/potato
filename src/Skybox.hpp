#ifndef POTATO_SKYBOX_H
#define POTATO_SKYBOX_H
#pragma once

#include "stdafx.h"
#include "Camera.hpp"

class Skybox
{
protected:
	void loadGeometry(void);
	void loadTexture(const std::string& skyname, const std::string& file, GLenum target);
	
	GLuint skyShader;
	GLuint viewUnif;
	GLuint projUnif;

	GLuint cubemap;
	GLuint vao;
	GLuint vbo;
	GLuint ebo;

	void setupCubeMap();
public:
	Skybox(void);

	void loadSkybox(const std::string& filename);
	void render(const glm::mat4& proj, Camera& cam);
	void clean(void);
};

#endif /* POTATO_SKYBOX_H */
