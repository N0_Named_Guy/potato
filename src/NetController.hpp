#ifndef POTATO_NET_CONTROLLER_H
#define POTATO_NET_CONTROLLER_H

#pragma once
#include "Controller.hpp"
#include "NetCommand.hpp"

class NetController :
	public Controller
{

public:
	NetController(void);
	~NetController(void);

	NetCommand* getCommand(void);
	void loadCommand(NetCommand& cmd);
};

#endif /* POTATO_NET_CONTROLLER_H */
