#include "TextureManager.hpp"

TextureManager::TextureManager(void) {
}

void TextureManager::clearTextures(void) {
	for (std::map<std::string, GLuint>::iterator it = textures.begin();
		it != textures.end(); it++) {
		
			glDeleteTextures(1, &(it->second));
	}
}

GLuint TextureManager::loadTexture(const std::string& texture) {
	return loadTexture(texture, false);
}

GLuint TextureManager::loadTexture(const std::string& texture, bool absolute) {
	return loadTexture(texture, absolute, SOIL_FLAG_INVERT_Y);
}

GLuint TextureManager::loadTexture(const std::string& texture, bool absolute, int SOILFlags) {
	GLuint tex = textures[texture];
	std::string path;

	if (!absolute) {
		path = TEXTURE_PATH + texture;
	} else {
		path = texture;
	}

	if (!tex) {
        int width, height;
        unsigned char* image;

        glGenTextures(1, &tex);
        glBindTexture(GL_TEXTURE_2D, tex);
		image = SOIL_load_image(path.c_str(), &width, &height, 0, SOIL_LOAD_RGBA);
		glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RGBA,
            width, height,
            0,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            image
        );
		SOIL_free_image_data(image);

		if (!tex) {
			d("<%s> Could not load texture\n", path.c_str());
			return 0;
		}

		textures[texture] = tex;
	}

	return tex;
}
