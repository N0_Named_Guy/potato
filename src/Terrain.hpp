#ifndef POTATO_TERRAIN_H
#define POTATO_TERRAIN_H
#pragma once

#include "HeightMap.hpp"

class Terrain :
	public HeightMap
{
protected:
	HeightMap** hmaps;

public:

	Terrain(void);
	~Terrain(void);
};

#endif /* POTATO_TERRAIN_H */
