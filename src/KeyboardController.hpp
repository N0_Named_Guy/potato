#ifndef POTATO_KEYBOARD_CONTROLLER_H
#define POTATO_KEYBOARD_CONTROLLER_H
#pragma once

#include "Controller.hpp"
#include "stdafx.h"

class KeyboardController:
	public Controller {
protected:
	SDLKey* controllerKeys;

public:
	virtual void init(void);
	virtual void loadControls(Json::Value& node);

	virtual void poll(void);
	virtual void clean(void);
};
#endif /* POTATO_KEYBOARD_CONTROLLER_H */
