#ifndef POTATO_HUD_H
#define POTATO_HUD_H
#pragma once

#include "stdafx.h"
#include "TextureManager.hpp"
#include "SurfaceManager.hpp"

enum GameState {
	playing,
	gameover,
	win,
	paused
};

struct HUDData {
	float speed;
	bool hit;
	float hp;
	bool checkpoint;
	GameState state;
};

class HUD {
protected:
	GLSurface surfPointer;
	GLSurface surfPointerBack;
	GLSurface surfGameOver;
	GLSurface surfHP;
	GLSurface surfHPBack;
	GLSurface surfCheckpoint;
	GLSurface surfWin;

	void renderSpeed(float speed, const glm::mat4& proj);
public:
	void setup(void);
	void render(HUDData& data);
};

#endif /* POTATO_HUD_H */

