#ifndef POTATO_UTILS_H
#define POTATO_UTILS_H
#pragma once

#include "stdafx.h"
#define USE_VBOS

#define BUFFER_OFFSET(i,t) ((void*)(i * sizeof(t)))
#define PIOVER180 0.0174532925f
#define TWOPI     6.28318531f

#define PLUSINFINITY (1.0f / 0.0f)
#define MINUSINFINITY (log(0.0))

#ifdef DEBUG
#define d(fmt, ...) \
	do { fprintf(stderr, "d@[%s:%d]:\n>> " fmt,__FUNCTION__, __LINE__, __VA_ARGS__); } while (0)
#else
#define d(fmt, ...)
#endif

extern float randnum (float min, float max);
// Makes a linear interpolation between a and b, by amount amt
extern float lerpf(float a, float b, float amt);

extern void vectorDiff(int d, const float* vec1, const float* vec2, float* ret);
extern void normalize(const int d, float* vec);
extern void calcNormal(const float triangle[3][3], float normal[3]);
extern GLuint createShader(GLenum shaderType, const std::string& filename);

extern Json::Value readJSONDoc(std::string file, Json::Reader& reader);
extern char* readTextFile(const std::string& filename);

extern void hexDump(const char *desc, const void* addr, const int len);

#endif /* POTATO_UTILS_H */
