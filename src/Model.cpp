#include "Model.hpp"
extern ShaderManager shaderManager;

Model::Model(void) {
	vao = 0;
	vbo = 0;
	ebo = 0;
}

Model::~Model(void) {

}

void Model::clean(void) {
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);
	
	glDeleteVertexArrays(1, &vao);
}

void applyMaterial(Material mat) {
	if (mat.tex > 0) {
		glActiveTexture(GL_TEXTURE0);
		
		glBindTexture(GL_TEXTURE_2D, mat.tex);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

		glEnable(GL_TEXTURE_2D);
	} else {
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_TEXTURE_2D);
	}
}

void Model::render(const glm::mat4& proj, const glm::mat4& view) {
	render(proj, view, glm::mat4());
}

void Model::render(const glm::mat4& proj, const glm::mat4& view, const glm::mat4& model) {
	glUseProgram(shader);
	glBindVertexArray(vao);
	
	// FIXME: quick material hack
	applyMaterial(materials[0]);

	glUniformMatrix4fv(projUnif, 1, GL_FALSE, glm::value_ptr(proj));
	glUniformMatrix4fv(viewUnif, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(modelUnif, 1, GL_FALSE, glm::value_ptr(model));

	glDrawElements(GL_TRIANGLES, nElements, GL_UNSIGNED_INT, (GLuint*)0+0);
}

void Model::buildVBOs() {
	// Create the vertex array object
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	shader = shaderManager.getProgram("model.json");
	glUseProgram(shader);

	GLsizeiptr buffSize;

	// Create the vertex buffer
	buffSize = sizeof(Vertex) * nVertices;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, buffSize, vertices, GL_STATIC_DRAW);
	//delete[] vertices;

	// Create the index buffer
	buffSize = sizeof(GLuint) * nElements;
	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffSize, elements, GL_STATIC_DRAW);
	//delete[] elements;

	// Set layout
	GLuint posAttrib = glGetAttribLocation(shader, "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE,
		8 * sizeof(GLfloat) + sizeof(GLuint), 0);

	GLuint normalAttrib = glGetAttribLocation(shader, "normal");
	glEnableVertexAttribArray(normalAttrib);
	glVertexAttribPointer(normalAttrib, 3, GL_FLOAT, GL_FALSE,
		8 * sizeof(GLfloat) + sizeof(GLuint), (void*)(3 * sizeof(GLfloat)));

	GLuint texCoordAttrib = glGetAttribLocation(shader, "texCoord");
	glEnableVertexAttribArray(texCoordAttrib);
	glVertexAttribPointer(texCoordAttrib, 2, GL_FLOAT, GL_FALSE,
		8 * sizeof(GLfloat) + sizeof(GLuint), (void*)(6 * sizeof(GLfloat)));

	// Get uniforms locations
	projUnif = glGetUniformLocation(shader, "proj");
	modelUnif = glGetUniformLocation(shader, "model");
	viewUnif = glGetUniformLocation(shader, "view");
	texUnif = glGetUniformLocation(shader, "texture");

	glUniform1i(texUnif, 0);


}
