TODO:

== In progress ==
* Player selection screen

== Bugs ==
* Fix terrain collision
	- rotate the points according to the unit's yaw.

== Improvements ==
* Improve graphical assets
* Improve sound system
* Add LOD to terrain
	- Subvide terrain into various grids (only index buffers needed)

== New Features ==
* Add multiplayer
* Add map selector
* Add credits
* Make a level editor
* Add a font system
* Add main menu
* Power ups

== Diary ==
20 Feb 2012
* Fixed entity pitching bug
* Very early work on networking

19 Feb 2012
* Initial work on the "dolphin" model

18 Feb 2012
* Moved configuration variables into their own global class
* Fixed interpolation bug, due to floating errors, by adding a "moving" state
to units

17 Feb 2012
* Sliding code complete. Fully dependent on gravity (as it should)
* Implemented Euler angles (yaw, pitch, roll).
	- Player now rolls while turning
* Touched the ambient values in the goals and the ship object
* Removed the rear mirror
* Made public release 4 (at http://omploader.org/vY3UwYg)
	- Made Facebook post
	- Made a LD post (at http://www.ludumdare.com/compo/2012/02/17/potato-project/)
* Moved the main loop to the Potato class, instead of every world have its own
loop.
	
16 Feb 2012
* Improved XZ terrain collision
	- Instead of checking height difference, check for the slope's angle.
* Begun work on player selection

15 Feb 2012
* Add json files for each type of vehicle (with their model and stats)
* Begun work on dynamic terrain LOD.
* Customizable controls via configuration file
* Messed with the XZ collision
* Initial work on "sliding". Downward slopes accelerate, upward slopes break

14 Feb 2012
* Added goal
