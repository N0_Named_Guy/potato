#version 330

uniform sampler2D heightMap;

void main () {
    vec4 v = vec4(gl_Vertex);
    v.y = texture2D(heightMap, gl_MultiTexCoord0.xy).x * 5.;
    gl_Position = gl_ModelViewProjectionMatrix * v;
}