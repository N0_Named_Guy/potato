#version 130

precision mediump float;

in vec3 TexCoord;

uniform samplerCube skyTex;

void main()
{
    gl_FragColor = textureCube(skyTex, TexCoord);
}
