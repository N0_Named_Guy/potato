#version 120

attribute vec3 position;
attribute vec3 normal;
attribute vec2 texCoord;

varying vec2 TexCoord;
varying vec3 Normal;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;

void main()
{
    gl_Position = proj * view * model * vec4(position, 1.0);
    TexCoord = texCoord;
    Normal = normal;
}