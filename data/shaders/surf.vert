#version 130

precision mediump float;

in vec2 position;
in vec2 texCoord;

out vec2 TexCoord;

uniform mat4 model;
uniform mat4 proj;

void main()
{
    gl_Position = proj * model * vec4(position, 0.0, 1.0);
    TexCoord = texCoord;
}