#version 130

precision mediump float;

in vec2 TexCoord;
in vec3 Normal;

uniform sampler2D texture;

void main()
{
    gl_FragColor = texture2D(texture, TexCoord);
}
