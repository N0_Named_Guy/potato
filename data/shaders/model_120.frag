#version 120

varying vec2 TexCoord;
varying vec3 Normal;

uniform sampler2D texture;

void main()
{
    gl_FragColor = texture2D(texture, TexCoord);
}
