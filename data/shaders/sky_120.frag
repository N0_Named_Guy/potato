#version 120

varying vec3 TexCoord;

uniform samplerCube skyTex;

void main()
{
    gl_FragColor = textureCube(skyTex, TexCoord);
}
